# Jenkins Pipeline with OpenShift

## Create Projects
    oc new-project segregated-hml
    oc new-project segregated-prd
    oc new-project automation

## Add permition to Jenkins Service Account

    oc adm policy add-cluster-role-to-user edit system:serviceaccount:automation:jenkins

## Deploy it on Openshift
    oc new-app  https://gitlab.com/openshift-samples/node-example2.git --strategy=pipeline -l template=node-example2 -n automation
